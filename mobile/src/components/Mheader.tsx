import React from 'react'
import './Mheader.less'
import logo from '../img/logo.png'
import {NavLink} from 'react-router-dom'
class Mheader extends React.Component{
    state:any;
    constructor(props:object){
         super(props)
         this.state = {
             ctStatus:'none'
         }
    }
    componentDidMount(): void {
        // let dom:any = document.getElementsByClassName('h-logo')[0];
    }

    render(){
        return (
            <div className={'mheader'}>
                <i className={'el-icon-menu h-menu'} onClick={()=>{
                   this.setState({
                        ctStatus:'block'
                    })
                    setTimeout(()=> {
                        let dom:any = document.getElementsByClassName('h-ct-con')[0]
                        dom.style.transform = 'translateX(0%)'
                        let alllist:any = document.getElementsByClassName('h-list')[0].getElementsByTagName('li')
                        for(let i of alllist){
                            i.style.transform = 'translateX(0%)'
                        }
                    },100)

                }}></i>
                明的博客
                <img src={logo} className={'h-logo'}/>

               <div className={'h-ct-bg'} style={{display:this.state.ctStatus}} onClick={()=>{
                    let dom:any = document.getElementsByClassName('h-ct-con')[0]
                    dom.style.transform = 'translateX(-100%)'
                    let alllist:any = document.getElementsByClassName('h-list')[0].getElementsByTagName('li')
                    for(let i of alllist){
                        i.style.transform = 'translateX(-100%)'
                    }
                    setTimeout(()=>{
                        this.setState({
                            ctStatus:'none'
                        })
                    },500)

                }}>
                    <div className={'h-ct-con'} onClick={(e)=>{
                        e.stopPropagation()
                    }}>
                        <img src={logo} className={'c-logo'} width={'80%'} style={{marginTop:'30px'}}/>
                        <ul className={'h-list'}>
                            <li><NavLink to={'/home'} activeClassName={'ac'}>首页</NavLink></li>
                            <li><NavLink to={'/jis'} activeClassName={'ac'}>技术文章</NavLink></li>
                            <li><NavLink to={'/codeLife'} activeClassName={'ac'}>程序人生</NavLink></li>
                            <li><NavLink to={'/life'} activeClassName={'ac'}>生活</NavLink></li>
                            <li><NavLink to={'/about'} activeClassName={'ac'}>关于我</NavLink></li>
                        </ul>
                    </div>
                </div>
            </div>
        )
    }
}
export default Mheader