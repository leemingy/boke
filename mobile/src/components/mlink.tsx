import React from 'react'
import axios from 'axios'
import base from '../util/paths'
import './mlink.less'
import {Layout} from 'element-react'
const Mlink:any = ()=>{
    let [links,setLink] = React.useState([])
    React.useEffect(()=>{
        axios.get(base+'/findLink').then((res)=>{
            let data = res.data;
            setLink(data.msg)
        })
    },[])
    console.log(links)
    return (<div className={'mlink'}>
        <div className={'link-head'}>
            友情链接
        </div>
        <div className={'link-con'}>
            <Layout.Row>
                {links.map((val:any,index)=>{
                    return(<Layout.Col span={'8'} style={{textAlign:'center'}} key={index}>
                        <a href={val.url} target={'blank'}>{val.name}</a>
                    </Layout.Col>)
                })}
            </Layout.Row>
        </div>
    </div>)
}

export default Mlink