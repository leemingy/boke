import React, {useEffect} from 'react'
import './acitem.less'
import {Button} from 'element-react'
import {HashRouter,Link} from "react-router-dom";

const AcItem:any =(props:any)=> {
    let [tit,setTit] = React.useState('')
    let [list,setList] = React.useState([50])
    useEffect(()=>{
        setTit(props.tit)
        setList(props.list)
    },[props])
    return(
        <HashRouter>
            <div className={'ac-item'}>
                <div className={'ac-head'}>
                    <span>{tit}</span>
                    <span>
                <Button size={'small'}><Link to={{pathname:"/more",search:'?type='+props.type+'&page=0'}}>更多<i className={'el-icon-caret-right'}></i></Link></Button>
            </span>
                </div>
                <ul className={'ac-list'}>
                    {list.map((val:any,index)=>{
                        return (<li key={index}><Link to={{pathname:'/des',search:'?type='+props.type+'&id='+val.id}}><span>{val.name}</span><span>{val.time}</span></Link> </li>)
                    })}
                </ul>
            </div>
        </HashRouter>)

}
export default AcItem