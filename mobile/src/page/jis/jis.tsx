import React, {useEffect} from 'react';
import './jis.less';
import Mheader from '../../components/Mheader'
import axios from 'axios'
import base from '../../util/paths'
import {Layout,Pagination} from 'element-react'
import {HashRouter,Link} from "react-router-dom";

const Jis: React.FC = (props:any) => {
  let [type,setType] = React.useState([])
  let [list,setList] = React.useState([])
  let [tot,setTot] = React.useState(0)
  let ur:any = props.location.search;
  let page:number = 0
  let mtype:number = 0
  if(sessionStorage.getItem('save')=='false'){
    try{
      page = parseInt(ur.match(/page=\d+/)[0].split('=')[1])
      mtype = ur.match(/type=[a-zA-Z0-9]+/)[0].split('=')[1]
    }catch(e){

    }

  }
  React.useEffect(()=>{
    axios.get(base+'/findType').then((res)=>{
      let data = res.data;
      setType(data.msg);
      if(sessionStorage.getItem('save')=='true'){
        props.history.push('/jis?page=0&type='+data.msg[0].id)
        // page = parseInt(ur.match(/page=\d+/)[0].split('=')[1])
        // mtype = ur.match(/type=[a-zA-Z0-9]+/)[0].split('=')[1]
        sessionStorage.setItem('save','false')
      }
    })
  },[])
  React.useEffect(()=>{
    try{
      page = parseInt(ur.match(/page=\d+/)[0].split('=')[1])
      mtype = ur.match(/type=[a-zA-Z0-9]+/)[0].split('=')[1]
      axios.get(base+'/findPageTArticle',{
        params:{
          num:7,
          page:page,
          type:mtype
        }
      }).then((res)=>{
        let data = res.data;
        setList(data.msg);
        setTot(data.total)
      })
    }catch (e) {

    }


  },[page,mtype])

  function setcon(con:string):string{
     let rcon = con.replace(/<(\/*\w+.*\d*)\/*>|(&\w+;)/g,'')
     let _rcon = rcon.substr(0,200)
    return _rcon
  }

  return (
      <HashRouter>
        <div className="App">
          <Mheader></Mheader>
          <div className={'jis-con'}>
            <div className={'jis-head'}>
              <Layout.Row >
                {type.map((val:any,index)=>{
                  return (
                      <Layout.Col className={'jis-h-col'} span={8} key={index}>

                 <span className={mtype==val.id?'choose':''} onClick={()=>{
                   props.history.push('/jis?page='+0+'&type='+val.id)
                 }}>{val.name}</span><span>({val.count})</span>
                      </Layout.Col>
                  )
                })}
              </Layout.Row>
            </div>
            <div className={'jis-ct'}>

              {list.length>0?(list.map((val:any,index)=>{
                return ( <Layout.Row key={index}>
                      <Layout.Col  span={4}>
                        <img src={base+val.pjname} alt="no img" width={'100%'} />
                      </Layout.Col>
                      <Layout.Col className={'ct-right'}  span={20}>
                        <div>
                          <Link  to={{pathname:'/des',search:"?type=newac&id="+val.id}}>
                            {setcon(val.con)}
                          </Link>

                        </div>

                      </Layout.Col>
                    </Layout.Row>
                )
              })):"该板块暂时没有文章！"}

            </div>
            <div className="block">
              <Pagination pageSize={7} currentPage={page} layout="prev, pager, next" total={tot} onCurrentChange={(page)=>{
                props.history.push('/jis?type='+mtype+'&page='+page)
              }}/>
            </div>
          </div>
        </div>
      </HashRouter>

  );
}

export default Jis;
