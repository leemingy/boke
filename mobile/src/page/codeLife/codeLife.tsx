import React from 'react';
import './codeLife.less';
import Mheader from '../../components/Mheader'
import base from "../../util/paths";
import axios from 'axios'
const CodeLife: React.FC = () => {
  React.useEffect(()=>{
    sessionStorage.setItem('save','true')
  })
  let [list,setList] = React.useState([])
  React.useEffect(()=>{
    axios.get(base+'/findCode').then((res)=>{
      let data = res.data;
      setList(data.msg)
    })
  },[])
  return (
      <div className="App">
        <Mheader></Mheader>
        <div className={'code-con'}>
          {list.map((val:any,index)=>{
            return(<div key={index} className={'life-con-item'}>
              <div className={'life-head'}>
                {val.name}
              </div>
              <div className={'life-cont'}>
                {val.con}
              </div>
            </div>)
          })}
        </div>
      </div>
  );
}

export default CodeLife;
