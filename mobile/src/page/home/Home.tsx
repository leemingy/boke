import React from 'react';
import './Home.less';
import Mheader from '../../components/Mheader'
import AcItem from '../../components/acitem'
import Mlink from '../../components/mlink'
import {Carousel} from 'element-react'
import base from '../../util/paths'
import axios from 'axios'
import {HashRouter,Link} from "react-router-dom";

const Home: React.FC = () => {
  let [banner,setbanner] = React.useState([])
  let [newac,setnewac] = React.useState([])
  let [codelist,setCodeList] = React.useState([])
  let [codelife,setCodeLife] = React.useState([])
  let [life,setLife] = React.useState([])
  let [gj,setGj] = React.useState([])
  React.useEffect(()=>{
      sessionStorage.setItem('save','true')

    axios.get(base+'/showBanner').then((res)=>{
      let data = res.data;
      setbanner(data.msg)
    })

    axios.get(base+'/findPageArticle',{
      params:{
        page:0,
        num:7
      }
    }).then((res)=>{
      let data = res.data;
      setnewac(data.msg)
    })
    axios.get(base+'/findPageShare',{
      params:{
        page:0,
        num:7
      }
    }).then((res)=>{
      let data = res.data;
      setCodeList(data.msg)
    })

    axios.get(base+'/findPageCode',{
      params:{
        page:0,
        num:7
      }
    }).then((res)=>{
      let data = res.data;
      setCodeLife(data.msg)
    })


    axios.get(base+'/findPageLife',{
      params:{
        page:0,
        num:7
      }
    }).then((res)=>{
      let data = res.data;
      setLife(data.msg)
    })

    axios.get(base+'/findPageGj',{
      params:{
        page:0,
        num:7
      }
    }).then((res)=>{
      let data = res.data;
      setGj(data.msg)
    })
  },[])


  return (
    <div className="App">
    <Mheader></Mheader>
      <Carousel interval="5000" arrow="always" height={'200px'}>
        {
          banner.map((item:any, index:number) => {
            return (
                <Carousel.Item key={index}>
                  <div>
                    <Link to={{pathname:'/des',search:"?type=banner&id="+item.id}}><img src={base+item.pjname} alt="" width={'100%'} height={'100%'}/></Link>
                  </div>

                </Carousel.Item>
            )
          })
        }
      </Carousel>
      <AcItem tit={'最新文章'} list={newac} type={'newac'}></AcItem>
      <AcItem tit={'编程分享'} list={codelist} type={'scode'}></AcItem>
      <AcItem tit={'程序人生'} list={codelife} type={'codelife'}></AcItem>
      <AcItem tit={'人活日记'} list={life} type={'life'}></AcItem>
      <AcItem tit={'工具集合'} list={gj} type={'gj'}></AcItem>
      <Mlink></Mlink>
    </div>
  );
}

export default Home;
