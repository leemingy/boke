import React, {useEffect} from 'react'
import './more.less'
import Mheader from '../../components/Mheader'
import axios from 'axios'
import base from '../../util/paths'
import {Pagination,Button} from 'element-react'
import {HashRouter,Link} from "react-router-dom";

const More:any = (props:any)=>{
    let [list,setList] = React.useState([])
    let [tot,setTot] = React.useState(0)
    let [name,setName] = React.useState('')
    let ur:any = props.location.search;
    let page:number = parseInt(ur.match(/page=\d+/)[0].split('=')[1])
    let type:string = ur.match(/type=[a-zA-Z0-9]+/)[0].split('=')[1]
    function setdata(text:string,url:string){
        setName(text)
        axios.get(base+url,{
            params:{
                page:page,
                num:7
            }
        }).then((res)=>{
            let data = res.data;
            setList(data.msg)
            setTot(data.total)
        })
    }
    function setInfo(){

        switch (type) {
            case 'newac':
                setdata('最新文章','/findPageArticle')
                break
            case 'scode':
                setdata('编程分享','/findPageShare')
                break
            case 'codelife':
                setdata('程序人生','/findPageCode')
                break
            case 'life':
                setdata('生活日记','/findPageLife')
                break
            case 'gj':
                setdata('工具集合','/findPageGj')
                break
        }
    }

    React.useEffect(()=>{
        setInfo()
    },[page])
    return (<HashRouter>
            <div className={'more'}>
                <Mheader></Mheader>
                <div className={'back'}>
                    <Button onClick={()=>{
                        props.history.replace('/home')
                    }}><i className={'el-icon-caret-left'}></i>返回</Button>
                </div>
                <div className={'m-con'}>
                    <div className={'m-header'}>
                        {name}
                    </div>
                    <div className={'m-cont'}>
                        {
                            list.map((val:any,index)=>{
                                return (<div key={index}>
                                    <Link to={{pathname:'/des',search:'?type='+type+'&id='+val.id}}>
                                    <span>{val.name}</span>
                                    <span>{val.time}</span>
                                    </Link>
                                </div>)
                            })
                        }
                    </div>
                </div>
                <div className="block">
                    <Pagination pageSize={7} currentPage={page} layout="prev, pager, next" total={tot} onCurrentChange={(page)=>{
                        props.history.push('/more?type='+type+'&page='+page)
                        page = page
                        // setInfo()
                    }}/>
                </div>
            </div>
        </HashRouter>
       )
}

export default More