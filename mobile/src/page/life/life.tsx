import React, {useEffect, useState} from 'react';
import './life.less';
import Mheader from '../../components/Mheader'
import base from '../../util/paths'
import axios from 'axios'
import {HashRouter,Link} from "react-router-dom";

let number:number = 5

function usePage(page:number,url:string) :any{
   let [list,setList] = React.useState([]);
   let [tot,setTot] = React.useState(0)
   useEffect(()=>{
     setPage(page)
   },[])
   function setPage(page:number) {
      axios.get(url,{
        params:{
          page:page,
          num:number
        }
      }).then((res)=>{
         let data = res.data;
         setTot(data.total)
        let rearr:any = [...list]
         for(let i of data.msg){
           rearr.push(i)
         }
         setList(rearr)
      })
   }
   return [list,tot,setPage]
}

const Life: React.FC = () => {
  React.useEffect(()=>{
    sessionStorage.setItem('save','true')
  })
    let [index,setIndex] = React.useState(1)
  React.useEffect(()=>{
    window.onscroll = function () {
       let dom:any = document.scrollingElement||document.documentElement;
       let scrollTop = dom.scrollTop;
       let scrollHeight:any = dom.scrollHeight;
       let wH = window.innerHeight;
       let totH:any = wH+scrollTop;
      if(Math.round((totH))>=Math.round(scrollHeight)-10&&Math.round((totH))<=Math.round(scrollHeight)+10){
        if(index*number>=tot){
          index = Math.ceil(tot/10)+10
        }else{
                setIndex(index+1)

        }
      }
    }
    return function () {
       window.onscroll = null
    }
  })

    React.useEffect(()=>{
        setList(index)
    },[index])
  let [list,tot,setList] = usePage(0,base+'/findPageLife')
  console.log(list)
  return (
      <HashRouter>
          <div className="App">
              <Mheader></Mheader>
              <div className={'life-con'}>
                  {list.map((val:any,index:any)=>{
                      return(<div key={index} className={'life-con-item'}>
                          <div className={'life-head'}>
                              {val.name}
                          </div>
                          <div className={'life-img'}>
                              <Link  to={{pathname:'/des',search:'?type=life'+'&id='+val.id}}>
                                  <img src={base+val.pjname} alt="no img"/>
                              </Link>

                          </div>
                      </div>)
                  })}
              </div>
          </div>
      </HashRouter>

  );
}

export default Life;
