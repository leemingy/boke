import React from 'react';
import './des.less';
import Mheader from '../../components/Mheader'
import base from '../../util/paths'
import axios from 'axios'
import back from '../../img/backt.svg'
import {Button} from 'element-react'
import {func} from "prop-types";
const Des: React.FC = (props:any) => {
  let [article,setArticle] = React.useState({
    name:'',
    con:"",
    time:""
  })

    let [sta,setsta] = React.useState('none')
  React.useEffect(()=>{
     let ur:any = props.location.search;
     let id:number = ur.match(/id=\d+/)[0].split('=')[1]
     let type:string = ur.match(/type=[a-zA-Z0-9]+/)[0].split('=')[1]
     switch (type) {
       case 'banner':
          axios.get(base+"/findBannerDes",{
            params:{
              id:id
            }
          }).then((res)=>{
            let data = res.data;
            setArticle(data.msg[0])
          })
         break;
         case 'newac':
             axios.get(base+"/findArticleDes",{
                 params:{
                     id:id
                 }
             }).then((res)=>{
                 let data = res.data;
                 setArticle(data.msg[0])
             })
             break;
         case 'scode':
             axios.get(base+"/findShareDes",{
                 params:{
                     id:id
                 }
             }).then((res)=>{
                 let data = res.data;
                 setArticle(data.msg[0])
             })
             break;
         case 'codelife':
             axios.get(base+"/findCodeDes",{
                 params:{
                     id:id
                 }
             }).then((res)=>{
                 let data = res.data;
                 setArticle(data.msg[0])
             })
             break;
         case 'life':
             axios.get(base+"/findLifeDes",{
                 params:{
                     id:id
                 }
             }).then((res)=>{
                 let data = res.data;
                 setArticle(data.msg[0])
             })
             break;
         case 'gj':
             axios.get(base+"/findGjDes",{
                 params:{
                     id:id
                 }
             }).then((res)=>{
                 let data = res.data;
                 setArticle(data.msg[0])
             })
             break;
     }
     window.onscroll = function(e:any){
         let dom:any = document.scrollingElement
        let top:number = dom.scrollTop;
        if(top>10){
            setsta('block')
        }else{
            setsta('none')
        }
     }
     return function(){
         window.onscroll = null
     }
  },[])
 function setcon():any{
    return {
      __html:article.con
    }
 }
  return (
    <div className="des">
    <Mheader></Mheader>
      <Button onClick={()=>{
        props.history.goBack()
      }} style={{marginTop:'10px',marginLeft:"10px"}}><i className={'el-icon-caret-left'}></i>返回</Button>
      <h2>{article.name}</h2>
      <div className={'des-con'} dangerouslySetInnerHTML={setcon()}>
      </div>
      <div className={'des-time'}>
        {article.time}
      </div>
        <img onClick={()=>{
            let dom:any = document.scrollingElement
           let t = setInterval(()=>{
               if( dom.scrollTop<=0){
                   console.log('d')
                   clearInterval(t)
               }
                dom.scrollTop=dom.scrollTop-15;
            })
        }} src={back} alt="no img" width={'40px'} style={{transition:'all 1s',position:'fixed',bottom:'100px',right:"30px",display:sta}}/>
    </div>
  );
}

export default Des;
