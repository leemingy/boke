import React, {useEffect} from 'react';
import logo from './logo.svg';
import './App.css';
import {HashRouter,Route,Switch,Redirect} from 'react-router-dom'
import Home from "./page/home/Home";
import About from "./page/about/About";
import CodeLife from "./page/codeLife/codeLife";
import Jis from "./page/jis/jis";
import Life from "./page/life/life";
import Des from "./page/des/des";
import More from "./page/more/more";
import axios from 'axios'
import gif from './img/timg.gif'
const App: React.FC = () => {
    let [show,setShow] = React.useState('none')
    axios.interceptors.request.use(
        config => {
          setShow('block')
            return config
        },
        err => {
            return Promise.reject(err)
        })
    axios.interceptors.response.use(
        config => {
            setShow('none')
            return config
        },
        err => {
            return Promise.reject(err)
        })
  return (
      <HashRouter>
          <div style={{position:'fixed',zIndex:5555,textAlign:'center',lineHeight:"100vh",left:0,top:0,bottom:0,width:'100%',backgroundColor:"rgba(7,17,27,0.2)",display:show}}>
              <img src={gif} alt="no img" width={'100px'} height={'100px'} style={{borderRadius:"10px"}}/>
          </div>
          <Switch>
              <Route path={'/'} exact component={Home}/>
              <Route path={'/home'} exact component={Home}/>
              <Route path={'/about'} component={About}/>
              <Route path={'/codeLife'} component={CodeLife}/>
              <Route path={'/jis'} component={Jis}/>
              <Route path={'/life'} component={Life}/>
              <Route path={'/des'} component={Des}/>
              <Route path={'/more'} component={More}/>
              {/*<Redirect to={'/home'}></Redirect>*/}
          </Switch>
      </HashRouter>
  );
}

export default App;
