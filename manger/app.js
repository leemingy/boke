const express = require('express');
const app = express();
const login = require('./model/loginR/index.js')
const addType = require('./model/addType/addType.js')
const article = require('./model/addarcticle/addarcticle.js')
const banner = require('./model/addBanner/addBanner.js')
const bg = require('./model/addbg/addBg.js')
const life = require('./model/addLife/addLife.js')
const code = require('./model/addCode/addCode.js')
const gj = require('./model/addGj/addGj.js')
const share = require('./model/addShare/addShare.js')
const user = require('./model/addUser/addUser.js')
const link = require('./model/addLink/addLink.js')
const kind = require('./model/kindUpload/kindUpload.js')

app.all("*",(req,res,next)=>{
    //设置允许跨域的域名，*代表允许任意域名跨域
    res.header("Access-Control-Allow-Origin","*");
    //允许的header类型
    res.header("Access-Control-Allow-Headers","content-type");
    //跨域允许的请求方式
    res.header("Access-Control-Allow-Methods","DELETE,PUT,POST,GET,OPTIONS");
    if (req.method.toLowerCase() == 'options'){
        res.send(200);  //让options尝试请求快速结束
    }else {
        next();
    }
})
app.use(express.static('./static'))
app.get('/login',login.login);
app.post('/addType',addType.addType);
app.get('/findType',addType.findType);
app.get('/editType',addType.editType);
app.get('/deleteType',addType.deleteType);
app.get('/deleteMany',addType.deleteMany);


app.post('/addArticle',article.addArticle);
app.get('/findArticle',article.findArticle);
app.get('/findArticleDes',article.findArticleDes);
app.get('/findPageArticle',article.findPageArticle);
app.get('/findPageTArticle',article.findPageTArticle);
app.post('/editArticle',article.editArticle);
app.get('/deleteArticle',article.deleteArticle);
app.get('/deleteArticleMany',article.deleteArticleMany);

app.post('/addBanner',banner.addBanner);
app.get('/findBanner',banner.findBanner);
app.get('/showBanner',banner.showBanner);
app.get('/findBannerDes',banner.findBannerDes);
app.post('/editBanner',banner.editBanner);
app.get('/deleteBanner',banner.deleteBanner);
app.get('/deleteBannerMany',banner.deleteBannerMany);
app.get('/cgStatus',banner.cgStatus)

app.post('/addBg',bg.addBg);
app.get('/findBg',bg.findBg);
app.post('/editBg',bg.editBg);
app.get('/deleteBg',bg.deleteBg);
app.get('/deleteBgMany',bg.deleteBgMany);


app.post('/addLife',life.addLife);
app.get('/findLife',life.findLife);
app.get('/findLifeDes',life.findLifeDes);
app.get('/findPageLife',life.findPageLife)
app.post('/editLife',life.editLife);
app.get('/deleteLife',life.deleteLife);
app.get('/deleteLifeMany',life.deleteLifeMany);


app.post('/addCode',code.addCode);
app.get('/findCode',code.findCode);
app.get('/findCodeDes',code.findCodeDes);
app.get('/findPageCode',code.findCodeLife)
app.post('/editCode',code.editCode);
app.get('/deleteCode',code.deleteCode);
app.get('/deleteCodeMany',code.deleteCodeMany);

app.post('/addGj',gj.addGj);
app.get('/findGj',gj.findGj);
app.get('/findGjDes',gj.findGjDes);
app.get('/findPageGj',gj.findPageGj)
app.post('/editGj',gj.editGj);
app.get('/deleteGj',gj.deleteGj);
app.get('/deleteGjMany',gj.deleteGjMany);


app.post('/addShare',share.addShare);
app.get('/findShare',share.findShare);
app.get('/findShareDes',share.findShareDes);
app.get('/findPageShare',share.findPageShare);
app.post('/editShare',share.editShare);
app.get('/deleteShare',share.deleteShare);
app.get('/deleteShareMany',share.deleteShareMany);


app.post('/addUser',user.addUser)
app.post('/editUser',user.editUser)
app.get('/findUser',user.findUser)



app.post('/addLink',link.addLink);
app.get('/findLink',link.findLink);
app.get('/editLink',link.editLink);
app.get('/deleteLink',link.deleteLink);
app.get('/deleteLinkMany',link.deleteMany);

app.post('/kindUpload',kind.addkind)

app.listen(3333)
