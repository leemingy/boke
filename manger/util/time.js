module.exports = {
    time:function () {
        let d = new Date();
        let y = d.getFullYear();
        let m = d.getMonth()+1;
        let date = d.getDate();
        let h = d.getHours()<10?'0'+d.getHours():d.getHours();
        let min = d.getMinutes()<10?'0'+d.getMinutes():d.getMinutes();
        let sec =  d.getSeconds()<10?'0'+d.getSeconds():d.getSeconds();
        return `${y}/${m}/${date} ${h}:${min}:${sec}`
    },
    tuTime:function () {
        let d = new Date();
        let y = d.getFullYear();
        let m = d.getMonth()+1;
        let date = d.getDate();
        let h = d.getHours()<10?'0'+d.getHours():d.getHours();
        let min = d.getMinutes()<10?'0'+d.getMinutes():d.getMinutes();
        let sec =  d.getSeconds()<10?'0'+d.getSeconds():d.getSeconds();
        return `${y}${m}${date}${h}${min}${sec}`
    }
}
