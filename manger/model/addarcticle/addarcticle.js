const mongo = require('../db/db.js')
const formidable = require('formidable')
const time = require('../../util/time.js')
const getId = require('../../util/getId.js')
const path = require('path')
const fs = require('fs')
module.exports = {
    addArticle:function (req,res) {
        var form = new formidable.IncomingForm();
        form.uploadDir = __dirname+'../../../static/uploadd'
        form.parse(req,function (err,files,file) {
            if(err){
                console.log(err)
                res.json({
                    status:500,
                    msg:err
                })
                return
            }

           let extname =  path.extname(file.file.name);
            let mtime = time.tuTime()
            fs.rename(file.file.path,__dirname+'/../../static/upload/'+mtime+extname,function (err) {
                if(err){
                    console.log(err)
                    res.json({
                        status:500,
                        msg:err
                    })
                    return
                }
                let pjname = '/upload/'+mtime+extname;
                let name = files.name;
                let type = parseInt(files.type);
                let con = files.con;
                getId.getId('articleId',function (id) {
                    mongo.insert('article',{id:id,name:name,type:type,con:con,pjname:pjname,time:time.time()},function () {
                        mongo.count('article',{type:type},function (number) {
                            mongo.update('type',{id:type},{$set:{count:number}},function () {
                                res.json({
                                    status:200,
                                    msg:'增加成功'
                                })
                            })
                        })

                    })
                })
            })
        })
    },
    editArticle:function (req,res) {
        var form = new formidable.IncomingForm();
        form.uploadDir = __dirname+'../../../static/uploadd'
        form.parse(req,function (err,files,file) {
            if(err){
                res.json({
                    status:500,
                    msg:err
                })
                return
            }
            let pjname = '';
            let name = files.name;
            let type = parseInt(files.type);
            let con = files.con;
            let id = parseInt(files.id);
            let mtime = time.tuTime()
           if(file.file=='undefined'||!file.file){
               pjname = files.file;
               mongo.update('article',{"id":id},{$set:{name:name,type:type,con:con,pjname:pjname,time:time.time()}},function () {
                   res.json({
                       status:200,
                       msg:'修改成功'
                   })
               })
           }else{
               let extname =  path.extname(file.file.name);
               fs.rename(file.file.path,__dirname+'/../../static/upload/'+mtime+extname,function (err) {
                   if(err){
                       res.json({
                           status:500,
                           msg:err
                       })
                       return
                   }
                  pjname = '/upload/'+mtime+extname;
                   mongo.update('article',{"id":id},{$set:{name:name,type:type,con:con,pjname:pjname,time:time.time()}},function () {
                       res.json({
                           status:200,
                           msg:'修改成功'
                       })
                   })

               })
           }


        })
    },
    findArticle:function (req,res) {
        mongo.find('article',{},{id:-1},function (result) {
            res.json({
                status:200,
                msg:result
            })
        })
    },
    findArticleDes:function (req,res) {
        let id = parseInt(req.query.id);
        mongo.find('article',{id:id},{id:-1},function (result) {
            res.json({
                status:200,
                msg:result
            })
        })
    },
    findPageArticle:function (req,res) {
        let page = parseInt(req.query.page)<=1?0:parseInt(req.query.page)-1;
        let num = parseInt(req.query.num);
        let tot = page*num;
        let search =  req.query.search;
        let json = search?{name:new RegExp(search)}:{}
        mongo.findPage('article',json,{id:-1},tot,num,function (result) {
            mongo.count('article',json,function (rnum) {
                res.json({
                    status:200,
                    msg:result,
                    total:rnum
                })
            })

        })
    },
    findPageTArticle:function (req,res) {
        let page = parseInt(req.query.page)<=1?0:parseInt(req.query.page)-1;
        let num = parseInt(req.query.num);
        let type = parseInt(req.query.type);
        let tot = page*num;
        let json = {type:type}
        mongo.findPage('article',json,{id:-1},tot,num,function (result) {
            mongo.count('article',json,function (rnum) {
                res.json({
                    status:200,
                    msg:result,
                    total:rnum
                })
            })

        })
    },
    deleteArticle:function (req,res) {
        let id =  parseInt(req.query.id);
        mongo.del('article',{"id":id},function () {
            res.json({
                status:200,
                msg:'删除成功'
            })
        })
    },
    deleteArticleMany:function (req,res) {
        let ids = req.query.ids.split(',');
        var index = 0
        for(let i=0;i<ids.length;i++){
            let id = parseInt(ids[i])
            mongo.del('article',{"id":id},function () {
                index++;
                if(index==ids.length-1){
                    res.json({
                        status:200,
                        msg:'删除成功'
                    })
                }

            })
        }

    }
}
