const mongo = require('../db/db.js')
const formidable = require('formidable')
const time = require('../../util/time.js')
const getId = require('../../util/getId.js')
const path = require('path')
const fs = require('fs')
module.exports = {
    addBg:function (req,res) {
        var form = new formidable.IncomingForm();
        form.uploadDir = __dirname+'../../../static/uploadd'
        form.parse(req,function (err,files,file) {
            if(err){
                res.json({
                    status:500,
                    msg:err
                })
                return
            }

           let extname =  path.extname(file.file.name);
            let mtime = time.tuTime()
            fs.rename(file.file.path,__dirname+'/../../static/upload/'+mtime+extname,function (err) {
                if(err){
                    res.json({
                        status:500,
                        msg:err
                    })
                    return
                }
                let pjname = '/upload/'+mtime+extname;
                getId.getId('bgId',function (id) {
                    mongo.insert('bg',{id:id,pjname:pjname,time:time.time()},function () {
                        res.json({
                            status:200,
                            msg:'增加成功'
                        })
                    })
                })
            })
        })
    },
    editBg:function (req,res) {
        var form = new formidable.IncomingForm();
        form.uploadDir = __dirname+'../../../static/uploadd'
        form.parse(req,function (err,files,file) {
            if(err){
                res.json({
                    status:500,
                    msg:err
                })
                return
            }
            let pjname = '';
            let id = parseInt(files.id);
            let mtime = time.tuTime()
           if(file.file=='undefined'||!file.file){
               pjname = files.file;
               mongo.update('bg',{"id":id},{$set:{pjname:pjname,time:time.time()}},function () {
                   res.json({
                       status:200,
                       msg:'修改成功'
                   })
               })
           }else{
               let extname =  path.extname(file.file.name);
               fs.rename(file.file.path,__dirname+'/../../static/upload/'+mtime+extname,function (err) {
                   if(err){
                       res.json({
                           status:500,
                           msg:err
                       })
                       return
                   }
                  pjname = '/upload/'+mtime+extname;
                   mongo.update('bg',{"id":id},{$set:{pjname:pjname,time:time.time()}},function () {
                       res.json({
                           status:200,
                           msg:'修改成功'
                       })
                   })

               })
           }


        })
    },
    findBg:function (req,res) {
        mongo.find('bg',{},{id:-1},function (result) {
            res.json({
                status:200,
                msg:result
            })
        })
    },
    deleteBg:function (req,res) {
        let id =  parseInt(req.query.id);
        mongo.del('bg',{"id":id},function () {
            res.json({
                status:200,
                msg:'删除成功'
            })
        })
    },
    deleteBgMany:function (req,res) {
        let ids = req.query.ids.split(',');
        var index = 0
        for(let i=0;i<ids.length;i++){
            let id = parseInt(ids[i])
            mongo.del('bg',{"id":id},function () {
                index++;
                if(index==ids.length-1){
                    res.json({
                        status:200,
                        msg:'删除成功'
                    })
                }

            })
        }

    }
}
