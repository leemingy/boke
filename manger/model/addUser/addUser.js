const mongo = require('../db/db.js')
const formidable = require('formidable')
const time = require('../../util/time.js')
const getId = require('../../util/getId.js')
const path = require('path')
const fs = require('fs')
module.exports = {
    addUser: function (req, res) {
        var form = new formidable.IncomingForm();
        form.uploadDir = __dirname + '../../../static/uploadd'
        form.parse(req, function (err, files, file) {
            if (err) {
                res.json({
                    status: 500,
                    msg: err
                })
                return
            }

            let extname = path.extname(file.file.name);
            let mtime = time.tuTime();
            let name = files.name;
            let phone = files.phone;
            let qq = files.qq;
            let gy = files.gy;
            let love = files.love;
            let address = files.address;
            let email = files.email;
            let work = files.work;
            fs.rename(file.file.path, __dirname + '/../../static/upload/' + mtime + extname, function (err) {
                if (err) {
                    res.json({
                        status: 500,
                        msg: err
                    })
                    return
                }
                let pjname = '/upload/' + mtime + extname;
                getId.getId('userId', function (id) {
                    mongo.insert('users', {id: id, pjname: pjname, time: time.time(),name:name,phone:phone,qq:qq,gy:gy,love:love,address:address,email:email,work:work}, function () {
                        res.json({
                            status: 200,
                            msg: '增加成功'
                        })
                    })
                })
            })
        })
    },
    editUser:function (req,res) {
        var form = new formidable.IncomingForm();
        form.uploadDir = __dirname+'../../../static/uploadd'
        form.parse(req,function (err,files,file) {
            if(err){
                res.json({
                    status:500,
                    msg:err
                })
                return
            }
            let pjname = '';
            let id = parseInt(files.id);
            let mtime = time.tuTime();
            let name = files.name;
            let phone = files.phone;
            let qq = files.qq;
            let gy = files.gy;
            let love = files.love;
            let address = files.address;
            let email = files.email;
            let work = files.work;
            if(file.file=='undefined'||!file.file){
                pjname = files.file;
                mongo.update('users',{"id":id},{$set:{pjname:pjname,time:time.time(),name:name,phone:phone,qq:qq,gy:gy,love:love,address:address,email:email,work:work}},function () {
                    res.json({
                        status:200,
                        msg:'修改成功'
                    })
                })
            }else{
                let extname =  path.extname(file.file.name);
                fs.rename(file.file.path,__dirname+'/../../static/upload/'+mtime+extname,function (err) {
                    if(err){
                        res.json({
                            status:500,
                            msg:err
                        })
                        return
                    }
                    pjname = '/upload/'+mtime+extname;
                    mongo.update('users',{"id":id},{$set:{pjname:pjname,time:time.time(),name:name,phone:phone,qq:qq,gy:gy,love:love,address:address,email:email,work:work}},function () {
                        res.json({
                            status:200,
                            msg:'修改成功'
                        })
                    })

                })
            }


        })
    },
    findUser:function (req,res) {
        mongo.find('users',{},{id:-1},function (result) {
            res.json({
                status:200,
                msg:result
            })
        })
    }

}
