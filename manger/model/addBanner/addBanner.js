const mongo = require('../db/db.js')
const formidable = require('formidable')
const time = require('../../util/time.js')
const getId = require('../../util/getId.js')
const path = require('path')
const fs = require('fs')
module.exports = {
    addBanner:function (req,res) {
        var form = new formidable.IncomingForm();
        form.uploadDir = __dirname+'../../../static/uploadd'
        form.parse(req,function (err,files,file) {
            if(err){
                res.json({
                    status:500,
                    msg:err
                })
                return
            }

           let extname =  path.extname(file.file.name);
            let mtime = time.tuTime()
            fs.rename(file.file.path,__dirname+'/../../static/upload/'+mtime+extname,function (err) {
                if(err){
                    res.json({
                        status:500,
                        msg:err
                    })
                    return
                }
                let pjname = '/upload/'+mtime+extname;
                let name = files.name;
                let con = files.con;
                getId.getId('bannerId',function (id) {
                    mongo.insert('banner',{id:id,name:name,con:con,pjname:pjname,time:time.time(),sta:0},function () {
                        res.json({
                            status:200,
                            msg:'增加成功'
                        })
                    })
                })
            })
        })
    },
    editBanner:function (req,res) {
        var form = new formidable.IncomingForm();
        form.uploadDir = __dirname+'../../../static/uploadd'
        form.parse(req,function (err,files,file) {
            if(err){
                res.json({
                    status:500,
                    msg:err
                })
                return
            }
            let pjname = '';
            let name = files.name;
            let con = files.con;
            let id = parseInt(files.id);
            let mtime = time.tuTime()
           if(file.file=='undefined'||!file.file){
               pjname = files.file;
               mongo.update('banner',{"id":id},{$set:{name:name,con:con,pjname:pjname,time:time.time()}},function () {
                   res.json({
                       status:200,
                       msg:'修改成功'
                   })
               })
           }else{
               let extname =  path.extname(file.file.name);
               fs.rename(file.file.path,__dirname+'/../../static/upload/'+mtime+extname,function (err) {
                   if(err){
                       res.json({
                           status:500,
                           msg:err
                       })
                       return
                   }
                  pjname = '/upload/'+mtime+extname;
                   mongo.update('banner',{"id":id},{$set:{name:name,con:con,pjname:pjname,time:time.time()}},function () {
                       res.json({
                           status:200,
                           msg:'修改成功'
                       })
                   })

               })
           }


        })
    },
    findBanner:function (req,res) {
        mongo.find('banner',{},{id:-1},function (result) {
            res.json({
                status:200,
                msg:result
            })
        })
    },
    showBanner:function (req,res) {
        mongo.find('banner',{sta:1},{id:-1},function (result) {
            res.json({
                status:200,
                msg:result
            })
        })
    },
    findBannerDes:function (req,res) {
        let id = parseInt(req.query.id)
        mongo.find('banner',{id:id},{id:-1},function (result) {
            res.json({
                status:200,
                msg:result
            })
        })
    },
    deleteBanner:function (req,res) {
        let id =  parseInt(req.query.id);
        mongo.del('banner',{"id":id},function () {
            res.json({
                status:200,
                msg:'删除成功'
            })
        })
    },
    deleteBannerMany:function (req,res) {
        let ids = req.query.ids.split(',');
        var index = 0
        for(let i=0;i<ids.length;i++){
            let id = parseInt(ids[i])
            mongo.del('banner',{"id":id},function () {
                index++;
                if(index==ids.length-1){
                    res.json({
                        status:200,
                        msg:'删除成功'
                    })
                }

            })
        }

    },
    cgStatus:function (req,res) {
        let id = parseInt(req.query.id);
        let num = parseInt(req.query.num);
        mongo.update('banner',{id:id},{$set:{sta:num}},function () {
            res.json({
                status:200,
                msg:'修改成功'
            })
        })
    }
}
