const mongo = require('../db/db.js')
const formidable = require('formidable')
const time = require('../../util/time.js')
const getId = require('../../util/getId.js')
const path = require('path')
const fs = require('fs')
module.exports = {
    addLife:function (req,res) {
        var form = new formidable.IncomingForm();
        form.uploadDir = __dirname+'../../../static/uploadd'
        form.parse(req,function (err,files,file) {
            if(err){
                res.json({
                    status:500,
                    msg:err
                })
                return
            }

           let extname =  path.extname(file.file.name);
            let mtime = time.tuTime()
            fs.rename(file.file.path,__dirname+'/../../static/upload/'+mtime+extname,function (err) {
                if(err){
                    res.json({
                        status:500,
                        msg:err
                    })
                    return
                }
                let pjname = '/upload/'+mtime+extname;
                let name = files.name;
                let con = files.con;
                getId.getId('lifeId',function (id) {
                    mongo.insert('life',{id:id,name:name,con:con,pjname:pjname,time:time.time()},function () {
                        res.json({
                            status:200,
                            msg:'增加成功'
                        })
                    })
                })
            })
        })
    },
    editLife:function (req,res) {
        var form = new formidable.IncomingForm();
        form.uploadDir = __dirname+'../../../static/uploadd'
        form.parse(req,function (err,files,file) {
            if(err){
                res.json({
                    status:500,
                    msg:err
                })
                return
            }
            let pjname = '';
            let name = files.name;
            let con = files.con;
            let id = parseInt(files.id);
            let mtime = time.tuTime()
           if(file.file=='undefined'||!file.file){
               pjname = files.file;
               mongo.update('life',{"id":id},{$set:{name:name,con:con,pjname:pjname,time:time.time()}},function () {
                   res.json({
                       status:200,
                       msg:'修改成功'
                   })
               })
           }else{
               let extname =  path.extname(file.file.name);
               fs.rename(file.file.path,__dirname+'/../../static/upload/'+mtime+extname,function (err) {
                   if(err){
                       res.json({
                           status:500,
                           msg:err
                       })
                       return
                   }
                  pjname = '/upload/'+mtime+extname;
                   mongo.update('life',{"id":id},{$set:{name:name,con:con,pjname:pjname,time:time.time()}},function () {
                       res.json({
                           status:200,
                           msg:'修改成功'
                       })
                   })

               })
           }


        })
    },
    findLife:function (req,res) {
        mongo.find('life',{},{id:-1},function (result) {
            res.json({
                status:200,
                msg:result
            })
        })
    },
    findLifeDes:function (req,res) {
        let id = parseInt(req.query.id)
        mongo.find('life',{id:id},{id:-1},function (result) {
            res.json({
                status:200,
                msg:result
            })
        })
    },
    findPageLife:function (req,res) {
        let page = parseInt(req.query.page)<=1?0:parseInt(req.query.page)-1;
        let num = parseInt(req.query.num);
        let tot = page*num;
        let search =  req.query.search;
        let json = search?{name:new RegExp(search)}:{}
        mongo.findPage('life',json,{id:-1},tot,num,function (result) {
            mongo.count('life',json,function (rnum) {
                res.json({
                    status:200,
                    msg:result,
                    total:rnum
                })
            })

        })
    },
    deleteLife:function (req,res) {
        let id =  parseInt(req.query.id);
        mongo.del('life',{"id":id},function () {
            res.json({
                status:200,
                msg:'删除成功'
            })
        })
    },
    deleteLifeMany:function (req,res) {
        let ids = req.query.ids.split(',');
        var index = 0
        for(let i=0;i<ids.length;i++){
            let id = parseInt(ids[i])
            mongo.del('life',{"id":id},function () {
                index++;
                if(index==ids.length-1){
                    res.json({
                        status:200,
                        msg:'删除成功'
                    })
                }

            })
        }

    }
}
