import React from 'react';
import './App.less';
import {HashRouter,Route,Switch,Redirect} from 'react-router-dom';
import Home from './component/home/home'
import Sy from "./component/sy/sy";
import {Loading} from 'element-react'
import axios from 'axios'
import Nopage from "./component/noPage/nopage";

function App() {
  const [sta,setSta] = React.useState(false)
//添加请求拦截器
    axios.interceptors.request.use(function(config){
        //在发送请求之前做某事
        setSta(true)
        return config;
    },function(error){
        //请求错误时做些事
        return Promise.reject(error);
    })

//添加响应拦截器
    axios.interceptors.response.use(function(response) {
        //对响应数据做些事
            setSta(false)
        return response
    },function(error){
        //请求错误时做些事
        return Promise.reject(error);
    })
  return (
    <div className="App">
       <HashRouter>
           {sta&&<Loading fullscreen={true} text={'拼命加载中...'}/>}
           <Switch>
              <Route path="/home" component={Home}></Route>
               <Route path="/" component={Home} exact></Route>
               <Route component={Sy} path='/sydd'/>
               <Route path="/noPage" component={Nopage}></Route>
               <Redirect to={'/noPage'}></Redirect>
           </Switch>

       </HashRouter>
    </div>
  );
}

export default App;
