import React from 'react'
import './header.less'
import {HashRouter,NavLink} from 'react-router-dom'
function Hheader() {
    return (<HashRouter>
            <div>
                <header className="home-head">
                    <img src="./img/logo.png" className="logo" width="150"/>
                    <ul>
                        <li><NavLink to='/home/sy' activeClassName='linkName'>首页</NavLink></li>
                        <li><NavLink to='/home/js' activeClassName='linkName'>技术文章</NavLink></li>
                        <li><NavLink to='/home/rs' activeClassName='linkName'>程序人生</NavLink></li>
                        <li><NavLink to='/home/life' activeClassName='linkName'>生活</NavLink></li>
                        <li><NavLink to='/home/user' activeClassName='linkName'>个人简历</NavLink></li>
                    </ul>

                </header>
            </div>
        </HashRouter>
       )
}
export default Hheader
