import React from 'react'
import './foot.less'
import axios from 'axios'
import {linkUrl} from "../../../utils/paths"
import {Layout} from 'element-react'
const useEffect =  React.useEffect;
const useState = React.useState;
function Foot() {
    let [uinfo,setUinfo]:[any,Function] = useState([]);
   useEffect(()=>{
        axios.get(linkUrl).then((res)=>{
            let data = res.data;
           setUinfo(data.msg)
        })
   },[])
    return (
        <div className="foot">
            Copyright ©2019 SSXfont
        </div>
   )
}
export default Foot
