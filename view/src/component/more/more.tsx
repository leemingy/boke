import React from 'react'
import {Pagination} from 'element-react'
import {HashRouter,Link} from 'react-router-dom'
import {moregj,cxPage,gjPage,codepage,lifePage} from '../../utils/paths'
import './more.less'
import axios from 'axios'
const useEffect =  React.useEffect;
const useState = React.useState;
function More(props) {
    let [info,setInfo] = useState([]);
    let [url,setUrl] = useState('');
    let [title,setTitle] = useState('');
    let [total,setTotal] = useState(0);
    useEffect(()=>{
       let s = props.location.search;
       let result = decodeURIComponent(s.split('=')[1]);
       setTitle(result);
       if(result=='今日动态'){
           // setUrl(moretd)
       }
        if(result=='编程分享'){
            setUrl(codepage)
        }
        if(result=='程序人生'){
            setUrl(cxPage)
        }
        if(result=='生活日记'){
            setUrl(lifePage)
        }
        if(result=='工具推荐'){
            setUrl(gjPage)
        }
    },[])
    useEffect(()=>{

        document.documentElement.scrollTop =0;
        console.log(url)
        if(url!=''){
            axios.get(url+'?page=1&num=10').then((res)=>{
                let data = res.data;
                setInfo(data.msg);
                console.log(data.total)
                setTotal(data.total)
            })
        }

    },[url])
    return (
        <HashRouter>
        <div id="more">
            <p style={{padding:"10px",color:"black"}}><span onClick={()=>{
                props.history.go(-1)
            }}><span className="el-icon-arrow-left"></span><span style={{marginLeft:"10px",marginTop:"10px"}}>返回上一级</span></span></p>

            <p className="more_head"><span>{title}</span></p>
        <div className="more_con">
            {info.map((val,index)=>{
                return (<div key={index} className="more_item">
                    <Link to={{pathname:"/home/des",search:"?id="+val.id+"&type="+title}}>
                    {val.name}
                    <span style={{float:'right'}}>{val.time}</span>
                    </Link>
                </div>)
            })}
        </div>
        <div className="more_page">
                <Pagination layout="total, prev, pager, next" total={total} pageSize={10} onCurrentChange={(page)=>{
                    axios.get(url+'?page='+page+'&num=10').then((res)=>{
                        let data = res.data;
                        setInfo(data.msg);

                    })
                }}/>
            </div>
        </div></HashRouter>)
}
export default More
