import React from 'react'
import {HashRouter,Link} from 'react-router-dom'
import './noPage.less'
function Nopage() {
   return (
       <HashRouter>
       <div className="error">
           <div className="container-floud">
               <div className="col-xs-12 ground-color text-center">
                   <div className="container-error-404">
                       <div className="clip">
                           <div className="shadow">
                               <span className="digit thirdDigit"></span>
                           </div>
                       </div>
                       <div className="clip">
                           <div className="shadow">
                               <span className="digit secondDigit"></span>
                           </div>
                       </div>
                       <div className="clip">
                           <div className="shadow">
                               <span className="digit firstDigit"></span>
                           </div>
                       </div>
                       <div className="msg">OH!
                           <span className="triangle"></span>
                       </div>
                   </div>
                   <h2 className="h1">很抱歉，你访问的页面找不到了</h2>
                   <p style={{textAlign:"center"}}>
                       <Link to={'/'}>返回首页</Link>
                   </p>
               </div>
           </div>
       </div>
       </HashRouter>)
}
export default Nopage
