import React from 'react'
import './js.less'
import {allUrl} from "../../utils/paths"
import {connect} from 'react-redux'
import Jaside from './jaside/jaside'
import All from '../sy/all/all'
const useEffect  = React.useEffect;
function Js({title,url}) {
    useEffect(()=>{
        document.documentElement.scrollTop =0;
    })
    return (<div id="js">
        <Jaside/>
        <div style={{'overflow':'auto'}}>
            <All url={url} title={title} showmore={false}/>
        </div>

    </div>)
}

const getstate = function (state) {
    return {
        title:state.jslist.title,
        url:state.jslist.url
    }
}
export default connect(getstate)(Js)
