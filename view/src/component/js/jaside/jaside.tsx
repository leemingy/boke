import React from 'react'
import './jaside.less'
import axios from 'axios'
import {varUrl} from "../../../utils/paths"
import {Layout} from 'element-react'
import {HashRouter,Link} from 'react-router-dom'
import {allUrl,hotUrl,pageType} from "../../../utils/paths"
import {store} from "../../../redux/store"
import {getjsList,setjslist} from "../../../redux/action/jslist"
import {setPage} from '../../../redux/action/setPage'
const useEffect =  React.useEffect;
const useState = React.useState;
function Jaside() {
    let [uinfo,setUinfo]:[any,Function] = useState([]);
    let [hinfo,setHinfo]:[any,Function] = useState([]);
   useEffect(()=>{
        axios.get(varUrl).then((res)=>{
            let data = res.data;
           setUinfo(data.msg);
           if(sessionStorage.getItem('msave')=='true'){
               sessionStorage.setItem('msave','false')
               store.dispatch(setPage(0));
               store.dispatch(setjslist(pageType+'?type='+data.msg[0].id,data.msg[0].name))
           }
            if(store.getState().jslist.title==''){
                sessionStorage.setItem('msave','false')
                store.dispatch(setPage(0));
                store.dispatch(setjslist(pageType+'?type='+data.msg[0].id,data.msg[0].name))
            }
           //
        })
       axios.get(allUrl).then((res)=>{
           let data = res.data;
           setHinfo(data.msg);
       })
   },[])
    return (
        <HashRouter>
        <div className="jaside animated  flipInY ">
            <p className="jaside_head"><span>文章分类</span></p>
            <Layout.Row className={'var_con'}>
                {uinfo.map((val,index)=>{
                    return (<Layout.Col span={'12'} className={'var_con_item'} key={index}>
                        <span onClick={()=>{store.dispatch(setPage(0));store.dispatch(getjsList(val.id,val.name))}}>{val.name}</span><span>({val.count?val.count:0})</span>
                    </Layout.Col>)
                })}
            </Layout.Row>
            <p className="jaside_head"><span>最新文章</span></p>
            <Layout.Row className={'var_con'}>
                {hinfo.map((val,index)=>{
                    return (<Layout.Col span={'24'} className={'var_con_item'} key={index}>
                        <span><Link to={{pathname:'/home/des',search:`?id=${val.id}&type=综合文章`}}>{val.name}</Link></span>
                    </Layout.Col>)
                })}
            </Layout.Row>
        </div>
        </HashRouter>
   )
}
export default Jaside
