import React from 'react'
import {Pagination} from 'element-react'
import {base,desUrl,bannerDes,codeDes,cxDes,lifeDes,alldes,gjDes} from '../../utils/paths'
import './des.less'
import axios from 'axios'
const useEffect =  React.useEffect;
const useState = React.useState;
function Des(props) {
    let [info,setInfo] = useState({name:'',time:'',con:''});
    let [type,setType] = useState('');
    let [id,setId] = useState(0);
    let [url,setUrl] = useState('')
    useEffect(()=>{
        document.documentElement.scrollTop =0;
        let s:string = props.location.search;
        let result:Array<any> = s.split('&');
        let id:number = result[0].split('=')[1];
        let type:string = decodeURIComponent(result[1].split('=')[1]);
        setType(type);
        setId(id);
        console.log(type)
        if(type=='banner'){
            setUrl(bannerDes)
        }
        if(type=='编程分享'){
            setUrl(codeDes)
        }
        if(type=='程序人生'){
            console.log(cxDes)
            setUrl(cxDes)
        }
        if(type=='生活日记'){
            setUrl(lifeDes)
        }
        if(type=='综合文章'){
            setUrl(alldes)
        }
        if(type=='最新文章'){
            setUrl(alldes)
        }
        if(type=='工具推荐'){
            setUrl(gjDes)
        }
    },[])
    useEffect(()=>{
        console.log(url)
        if(url){
            axios.get(url,{
                params:{
                    id:id
                }
            }).then((res)=>{
                let data = res.data;
                setInfo(data.msg[0]);
            })
        }

    },[url])
    let obj:any = {
        __html:info.con
    }
    return (<div id="Des">
        <p style={{paddingTop:"20px"}}><span onClick={()=>{
           props.history.go(-1)
        }}><span className="el-icon-arrow-left"></span><span style={{marginLeft:"10px",marginTop:"10px"}}>返回上一级</span></span></p>
        <p className={'des_title'}>{info.name}</p>
        <div className={'des_con'} dangerouslySetInnerHTML={obj}></div>
        <div className={'des_time'}>{info.time}</div>
        <img src="./img/backt.svg" className="backT" onClick={()=>{
            var t =  setInterval(()=>{
                if(document.documentElement.scrollTop==0){
                    document.documentElement.scrollTop=0;
                    clearInterval(t)
                }
                document.documentElement.scrollTop-=60;
            },10)

        }}/>
    </div>)
}
export default Des
