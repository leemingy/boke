import React from 'react'
import {Layout} from 'element-react'
import {cxall} from '../../utils/paths'
import './rs.less'
import axios from 'axios'
const useEffect =  React.useEffect;
const useState = React.useState;
function Rs() {
    let [info,setInfo] = useState([]);
    useEffect(()=>{
        document.documentElement.scrollTop =0;
        sessionStorage.setItem('msave','true')
        axios.get(cxall).then((res)=>{
            let data = res.data;
            setInfo(data.msg);
        })
    },[])

    return (<div id="rs">
         <Layout.Row>
             {info.map((val,index)=>{
                 return (<Layout.Col span={'24'} className={'rs_col  animated  bounceInDown'} key={index}>
                     <div className="rs_item">
                         {val.name}
                         <p style={{textAlign:'left'}}>{val.con}</p>
                         <p>{val.time}</p>
                     </div>
                     <img src="./img/jt.svg" width="50px" className="rs_img"/>
                 </Layout.Col>)
             })}
         </Layout.Row>
        <div className="rs_item">
        待续.....
    </div>
        </div>)
}
export default Rs
