import React from 'react'
import './user.less'
function Muser(){
  return (
      <div className="user  animated  rollIn ">
          <base target="_blank"/>
         <h2 className="u-center">
             李明月
         </h2>
          <div className="u_phone u-center">
              +86 15923870940 | 2411923851@100qq.com
          </div>
          <div className="u-center">
              <span>地址：重庆市云阳县 </span><span>学历：本科(重庆邮电大学移通学院)</span>
          </div>
          <div className="u-center">
              <span>个人网站：<a href="http://leeminy.cn:3333">http://leeminy.cn:3333</a> </span><span>码云：<a href="https://gitee.com/leemingy">https://gitee.com/leemingy</a></span>
          </div>
          <div className={'u-con'}>
              <div className="u-con-item">
                  <h2>技能</h2>
                  <hr/>
                  <ul className="u-con-item-con">
                      <li>熟悉html5,css3,响应式开发，移动端自适应开发。</li>
                      <li>熟悉原生js,深刻理解原型，闭包，作用域，event-loop，垃圾回收机制等，熟悉常用的设计模式。</li>
                      <li>熟练运用es6+,熟悉typescript,。</li>
                      <li>熟悉vue全家桶，vue+vue-router+vuex，能配合element-ui进行快速开发。</li>
                      <li>熟悉react,redux,react-hooks，react-router,能配合element-ui进行快速开发。</li>
                      <li>熟悉webpack,less。熟悉git常用操作</li>
                      <li>熟悉three.js,node.js,uniapp跨端开发。</li>
                  </ul>
              </div>
              <div className="u-con-item">
                  <h2>工作与实习经历</h2>
                  <hr/>
                  <div className="u-con-item-con">
                      <div className="u-con-item-con-item">
                          <div className="u-con-head">
                              <span>重庆野海科技有限公司</span>
                              <span>2017年9月 - 2018年2月</span>
                          </div>
                          <div className="u-con-gs-info">
                              负责web端的页面开发及其对接后台接口，主要商城开发，官网开发，后台管理平台开发，涉及pc端，移动端以及公众号开发。
                          </div>
                      </div>
                      <div className="u-con-item-con-item">
                          <div className="u-con-head">
                              <span>重庆木棉树软件开发 </span>
                              <span>2018年4月-2019年9月</span>
                          </div>
                          <div className="u-con-gs-info">
                             <ul>
                                 <li>负责页面开发，包括pc,移动端，webapp的开发；</li>
                                 <li>负责公司相关技术研究，例如图像处理，视频通话，手势操控，网络监控等。</li>
                                 <li>利用vue重构旧代码。</li>
                                 <li>利用three.js进行一些三维场景开发：服装定制，攻防平台，展厅，管理平台展示。</li>
                             </ul>
                                  </div>
                      </div>
                  </div>
              </div>

              <div className="u-con-item">
                  <h2>项目经验</h2>
                  <hr/>
                  <div className="u-con-item-con">
                      <div className="u-con-item-con-item">
                          <div className="u-con-head">
                              <span><a href="https://imzhuomo.com">爱镯魔网络工厂</a></span>
                              <span>2018.4 - 2018.8</span>
                          </div>
                          <div className="u-con-gs-info">
                              <div>
                                  采用技术：vue+vue-cli+vue-router+vuex+webpack+es6+canvas+three.js.
                              </div>
                              <div>
                                  项目描述：项目用vue-cli开启项目，项目有pc,移动,app三版，项目板块主要有用户三维可视化定制手镯，定制成功后，用户可以加入馆藏，馆藏得作品可以供他人修改后再买，或者他人直接购买，用户可以把购买后的作品进行回收处理，当然也有在线商城不用定制直接购买现成作品。
                              </div>
                          </div>
                      </div>
                      <div className="u-con-item-con-item">
                          <div className="u-con-head">
                              <span>重庆智慧小区平台(该项目只在智博会展示没有上线地址)   </span>
                              <span>2019.8 - 2019.8</span>
                          </div>
                          <div className="u-con-gs-info">
                              <div>
                                  采用技术：vue+vue-cli+escharts+vue-router+webpack+es6+three.js.
                              </div>
                              <div>
                                  项目描述：项目主要用三维的方式来展示出重庆各个板块的小区分布。以及对应的四个小区的相关信息，采用escharts来展现小区的信息，如人流量，水电消耗，入住率，周边情况等。该项目在重庆的智博会进行了展示。    </div>
                          </div>
                      </div>

                      <div className="u-con-item-con-item">
                          <div className="u-con-head">
                              <span><a href="http://leeminy.cn:3333/ctf/test.html">CTF网络安全比赛平台展示</a></span>
                              <span> 2019.6-2019.7</span>
                          </div>
                          <div className="u-con-gs-info">
                              <div>
                                  采用技术：three.js
                              </div>
                              <div>
                                  项目描述：项目主要是一个用来展示学生们对网络攻防比赛的一个答题情况的展示。采用three.js来实现一个可视化的炫酷展示效果。展示了用户答题成功或者失败的效果，能清晰的观察到排行榜的情况，某个关卡的前三名，以及前几个作答成功的提示。当然这是ctf模式，此外还有红蓝对抗模式，即没有关卡，用户自身之间的对战。   </div>
                      </div>
                  </div>
              </div>
          </div>
  </div>
      </div>
  )
}

export default Muser