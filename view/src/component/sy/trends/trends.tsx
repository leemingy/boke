import React from 'react'
import './trends.less'
import {HashRouter,Link} from 'react-router-dom'
import axios from 'axios'
const useEffect =  React.useEffect;
const useState = React.useState;
function Trends(props) {
    let [uinfo,setUinfo]:[any,Function] = useState([]);
   useEffect(()=>{
        axios.get(props.url).then((res)=>{
            let data = res.data;
           setUinfo(data.msg)
        })
   },[])
    return (
        <HashRouter>
        <div className="trends animated  bounceInDown" style={{width:(props.title=='工具推荐'?'100%':'calc(50% - 5px)')}}>
            <p className="trends_head"><span>{props.title}</span><span><Link to={{pathname:'/home/more',search: '?title='+props.title}}>{uinfo.length>=7&&props.title!='最新文章'?'更多':''}</Link></span></p>
            <div className="trends_content">
                {uinfo.length>0?uinfo.map((val,index)=>{
                    return (<div className="trends_item" key={index}><Link to={{pathname:'/home/des',search:`?id=${val.id}&type=${props.title}`}}><span>{val.name}</span><span>{val.time}</span></Link></div>)
                }):(<div>博主没有动态!</div>)}
            </div>
        </div>
        </HashRouter>
  )
}
export default Trends
