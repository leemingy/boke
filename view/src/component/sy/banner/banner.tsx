import React from 'react'
import './banner.less'
import {Carousel} from 'element-react'
import {connect} from 'react-redux'
import {HashRouter,Link} from 'react-router-dom'
import {base} from '../../../utils/paths'
import {bannerlist} from '../../../redux/action/bannerlist'
const useEffect =  React.useEffect;
function Banner({bannerlist,setbannerlists}) {
   useEffect(()=>{
       setbannerlists()
   },[])
    return (<HashRouter>
        <div className="banner animated  rollIn delay-1s">
            <Carousel  interval={'5000'}>
                {bannerlist.map((val,index)=>{
                    return (<Carousel.Item key={index}>
                        <Link to={{pathname:'/home/des',search:'?id='+val.id+"&type=banner"}}>
                            <div className="banner_zz">

                            </div>
                        <img src={base+val.pjname}/>
                        <h1 className="banner_tit">{val.name}</h1>
                        </Link>
                    </Carousel.Item>)
                })}
            </Carousel>
        </div>
    </HashRouter>)
}
const getState = function (state) {
    return {
        bannerlist:state.bannerlist
    }
}
const getaction = function (dispacth) {
    return{
        setbannerlists:function () {
            dispacth(bannerlist())
        }
    }
}
export default connect(getState,getaction)(Banner)
