import React from 'react'
import './minfo.less'
import axios from 'axios'
import {base,uinfoUrl} from "../../../utils/paths"
const useEffect =  React.useEffect;
const useState = React.useState;
function Minfo() {
    let [uinfo,setUinfo] = useState({
        name:'',
        qq:'',
        phone:"",
        email:"",
        address:"",
        work:"",
        love:'',
        pjname:"",
        gy:''
    });
   useEffect(()=>{
        axios.get(uinfoUrl).then((res)=>{
            let data = res.data;
           setUinfo(data.msg[0])
        })
   },[])
    return (
        <div className="minfo animated  bounceInDown ">
          <p style={{"textAlign":"center","borderBottom":"1px solid #A8ABAD","padding":"10px"}}><img src={base+uinfo.pjname} className="uinfo_head animated  flipInY delay-1s"/></p>
           <div className="uinfo_content">
               <div className="uinfo_item"><span>姓名:</span><span>{uinfo.name}</span></div>
               <div className="uinfo_item"><span>qq:</span><span>{uinfo.qq}</span></div>
               <div className="uinfo_item"><span>电话:</span><span>{uinfo.phone}</span></div>
               <div className="uinfo_item"><span>邮件:</span><span>{uinfo.email}</span></div>
               <div className="uinfo_item"><span>地址:</span><span>{uinfo.address}</span></div>
               <div className="uinfo_item"><span>职位:</span><span>{uinfo.work}</span></div>
               <div className="uinfo_item"><span>爱好:</span><span>{uinfo.love}</span></div>
               <div className="uinfo_item"><span>格言:</span><span>{uinfo.gy}</span></div>
           </div>
        </div>
   )
}
export default Minfo
