import React from 'react'
import './all.less'
import {Layout,Pagination} from 'element-react'
import axios from 'axios';
import {HashRouter,Link} from 'react-router-dom'
import {base,pageType} from '../../../utils/paths'
import {connect} from 'react-redux'
import {setPage} from '../../../redux/action/setPage'
const useEffect =  React.useEffect;
const useState = React.useState;
function All(props) {
    let [alinfo,setAlinfo] = useState([]);
    let [count,setcount] = useState(0);
   useEffect(()=>{
       if(props.url){
           axios.get(props.url+'&num=10&page='+props.page).then((res)=>{
               let data = res.data;
               setAlinfo(data.msg)
               if(data.hasOwnProperty('total')){
                   setcount(data.total)
               }
           })
       }

   },[props.url])
  function sethtml(html) {
      return{
          __html:html
      }
  }

  function setcon(con):any{
      let rcon = con.replace(/<(\/*\w+.*\d*)\/*>|(&\w+;)/g,'')
      let _rcon = rcon.substr(0,200)
    return _rcon
  }

    return (
        <HashRouter>
        <div className="ALL animated  flipInY ">
         <Layout.Row>
             <Layout.Col span='24' className="Col">
                 <p className="all_head"><span>{props.title}</span>
                     {props.showmore?<span><Link to='/home/js'>更多</Link></span>:''}
                     </p>
                 <div className="all_content">
                     {
                         alinfo.length>0? alinfo.map((val,index)=>{
                                 return (<div className="all_con_item" key={index}>
                                     <div className="all_left">
                                         <img src={base+val.pjname}/>
                                     </div>
                                     <div className="all_right">
                                         <Link to={{pathname:'/home/des',search:`?id=${val.id}&type=综合文章`}}>
                                         <p>{val.name}</p>
                                         <div>{setcon(val.con)}</div>
                                         <p><span style={{float:"right"}}>{val.time}</span></p>
                                         </Link>
                                     </div>
                                 </div>)
                             }):<div className="no">博主暂时没有发布文章！</div>



                     }
                     <div className="all_page">
                         {props.showmore?'': <Pagination layout="total, prev, pager, next" currentPage={props.page} total={count} pageSize={10} onCurrentChange={(page)=>{
                            props.spage(page);
                             axios.get(props.url+'&num=10&page='+page).then((res)=>{
                                 let data = res.data;
                                 setAlinfo(data.msg)
                             })
                         }}/>}

                     </div>
                 </div>
             </Layout.Col>
         </Layout.Row>
        </div>
        </HashRouter>
    )
}
const getState = function (state) {
    return {
        page:state.page
    }
}

const getAction = function (dispatch) {
    return{
        spage:function (num) {
            dispatch(setPage(num))
        }
    }
}

export default connect(getState,getAction)(All)
