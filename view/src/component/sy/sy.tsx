import React from 'react'
import './sy.less'
import {Carousel} from 'element-react'
import {connect} from 'react-redux'
import {bannerlist} from '../../redux/action/bannerlist'
import Banner from './banner/banner'
import Minfo from './minfo/minfo'
import Trends from './trends/trends'
import All from './all/all'
import Links from './link/link'
import {trendUrl,lifeUrli,allUrl,codeUrl,cxUrl,lifeUrl,gjUrl} from "../../utils/paths"
const useEffect =  React.useEffect;
function Sy() {
   useEffect(()=>{
       document.documentElement.scrollTop =0;
       sessionStorage.setItem('msave','true')
   },[])
    return (<div>
        <div className="sy">
            <Banner className={''}></Banner>
            {/*<div className="iT">*/}
                {/*/!*<Minfo></Minfo>*!/*/}
                {/*<Banner></Banner>*/}
                {/*<Trends title={'最新文章'} url={allUrl}></Trends>*/}
            {/*</div>*/}
            <Trends title={'最新文章'} url={allUrl}></Trends>
            <Trends title={'编程分享'} url={codeUrl}></Trends>
            <Trends title={'程序人生'} url={cxUrl}></Trends>
            <Trends title={'生活日记'} url={lifeUrli}></Trends>
            <Trends title={'工具推荐'} url={gjUrl}></Trends>
            <Links></Links>
        </div>
    </div>)
}
export default Sy
