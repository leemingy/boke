import React from 'react'
import './link.less'
import axios from 'axios'
import {linkUrl} from "../../../utils/paths"
import {Layout} from 'element-react'
const useEffect =  React.useEffect;
const useState = React.useState;
function Link() {
    let [uinfo,setUinfo]:[any,Function] = useState([]);
   useEffect(()=>{
        axios.get(linkUrl).then((res)=>{
            let data = res.data;
           setUinfo(data.msg)
        })
   },[])
    return (
        <div className="link">
            <p className="link_head"><span>友情链接</span></p>
             <Layout.Row>
                 {uinfo.map((val,index)=>{
                     return (<Layout.Col span='6' key={index} className={'link_col'}>
                         <span><a href={val.url}>{val.name}</a></span>
                     </Layout.Col>)
                 })}

             </Layout.Row>
        </div>
   )
}
export default Link
