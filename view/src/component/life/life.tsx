import React from 'react'
import {Layout} from 'element-react'
import './life.less';
import {base,lifeUrl} from '../../utils/paths'
import {HashRouter,Link} from 'react-router-dom'
import axios from 'axios'
import Waterfall from '../public/waterfall/waterfall'
const useEffect =  React.useEffect;
const useState = React.useState;

function Life() {

    const [list,setlist] = useState([])
    useEffect(()=>{
        document.documentElement.scrollTop =0;
        sessionStorage.setItem('msave','true')
        axios.get(lifeUrl).then((res)=>{
            let data = res.data;
            setlist(data.msg)
        })
    },[])
    console.log(list)
    return (
        <HashRouter>
        <div id="life">
            <Layout.Row>
                {list.length>0?(<Waterfall data={list} col={4} gap={5} temp={(item)=> {
                    return (
                        <div className="life_wai">
                            <Link to={{pathname: '/home/des', search: `?id=${item.id}&type=生活日记`}}>
                                <img src={base + item.pjname} width="100%" />
                                <div className="life_zz">{item.name}</div>
                            </Link>
                        </div>)
                }}>
                </Waterfall>):""}

            </Layout.Row>
        </div>
        </HashRouter>)
}
export default Life
