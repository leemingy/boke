import React from 'react';
import './home.less'
import axios from 'axios'
import scroll from '../../utils/scroll.js'
import Bgscroll from './bgscroll'
import Hheader from '../public/header/header'
import {HashRouter,NavLink,Route,Switch,Redirect} from 'react-router-dom'
import {connect} from 'react-redux'
import {getBgList} from '../../redux/action/listbg'
import Sy from "../sy/sy";
import Rs from "../rs/rs";
import Life from "../life/life";
import About from "../about/about";
import Js from "../js/js";
import Foot from '../public/foot/foot'
import More from "../more/more";
import Des from "../des/des";
import Muser from "../user/user";
import Minfo from '../sy/minfo/minfo'
const useState = React.useState;
const useEffect =  React.useEffect;
const useCallback = React.useCallback;
const useRef = React.useRef;
function Home() {
    //声明背景图片数据
    // let [bglist,setbgList]:[Array<string>,Function] = useState([]);
    let contentRef = useRef();
    //开始获取数据

    //渲染模板
    return (
            <div id="home">
                {/*<Bgscroll list={gbglist}/>*/}
                <div  className="home-warp" ref={contentRef}>
                    <Bgscroll/>
                    <Hheader></Hheader>
                    <Minfo></Minfo>
                    <div className="home-content" >

                        <Switch>
                            <Route component={Sy} path='/home/sy'/>
                            <Route component={Rs} path='/home/rs'/>
                            <Route component={Life} path='/home/life'/>
                            <Route component={About} path='/home/about'/>
                            <Route component={Js} path='/home/js'/>
                            <Route component={More} path='/home/more'/>
                            <Route component={Des} path='/home/des'/>
                            <Route component={Muser} path='/home/user'/>
                            <Redirect to='/home/sy'/>
                        </Switch>

                    </div>
                    <Foot></Foot>
                </div>
            </div>

       )
}

export default Home
