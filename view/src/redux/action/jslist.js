import axios from 'axios'
import {pageType} from "../../utils/paths";
const JSLIST = 'JSLIST'
const url = pageType;
const getjsList = function (id,tit) {
    return function (dispatch) {
            dispatch(setjslist(url+'?type='+id,tit))
    }
}
const setjslist=function (data,tit) {
    return {
        type:JSLIST,
        data:data,
        title:tit
    }
}
export {getjsList,setjslist}
