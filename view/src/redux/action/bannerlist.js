import axios from 'axios'
import {banerurl} from "../../utils/paths";
const BANNERLIST = 'BANNERLIST'
const url = banerurl;
const bannerlist = function () {
    return function (dispatch) {
        axios.get(url).then((res)=>{
            let data = res.data;
            dispatch(setbannerlist(data.msg))
        })
    }
}
const setbannerlist=function (data) {
    return {
        type:BANNERLIST,
        data:data
    }
}
export {bannerlist}
