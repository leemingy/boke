import axios from 'axios'
import {bgurl} from "../../utils/paths";
const BGLIST = 'BGLIST'
const url = bgurl;
const getBgList = function () {
    return function (dispatch) {
        axios.get(url).then((res)=>{
            let data = res.data;
            dispatch(setbglist(data.msg))
        })
    }
}
const setbglist=function (data) {
    return {
        type:BGLIST,
        data:data
    }
}
export {getBgList}
