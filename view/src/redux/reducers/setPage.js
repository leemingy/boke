import {state} from '../store'
const page = function (states=state.page,action) {
    if(action.type=='SETPAGE'){
        return action.page
    }
    return states;
}
export default page
