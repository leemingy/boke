import {state} from '../store'
const jslist = function (states=state.jslist,action) {
    if(action.type=='JSLIST'){
        return {...states,title:action.title,url:action.data}
    }
    return states;
}
export default jslist
