import {state} from '../store'
const bannerlist = function (states=state.bannerlist,action) {
    if(action.type=='BANNERLIST'){
        return action.data
    }
    return states;
}
export default bannerlist
