import {state} from '../store'
const listBg = function (states=state.listBg,action) {
    if(action.type=='BGLIST'){
        return action.data
    }
    return states;
}
export default listBg
