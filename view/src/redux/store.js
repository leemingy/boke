import {createStore,applyMiddleware,combineReducers} from 'redux'
import reduxthunk from 'redux-thunk'
import listBg from './reducers/listbg.js'
import bannerlist from './reducers/bannerlist'
import jslist from './reducers/jslist'
import page from './reducers/setPage'
const state = {
    listBg:[],
    bannerlist:[],
    jslist:{
        title:'',
        url:''
    },
    page:1
}
const reducer = combineReducers({
    listBg,
    bannerlist,
    jslist,
    page
})
const store = createStore(reducer,applyMiddleware(reduxthunk))
export {state,store}
