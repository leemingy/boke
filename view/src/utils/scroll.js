export default function scroll(contentRef,bglist) {
    let top=null,index=null,imgs=null,h=null;
    top?top ='':top = document.documentElement.scrollTop;
    index?index='':index = 0;
    imgs?imgs="":imgs = document.getElementsByClassName('bg_img');
    h?h = '':h=window.innerHeight;
    for(let i=0;i<bglist.length;i++){
        if(top>=i*h&&top<(i+1)*h){
            index = i;
           for(let j=0;j<imgs.length;j++){
               imgs[j].style.top = -h+'px'
           }
           try{
               if(index<bglist.length-1){
                   imgs[index+1].style.top = 0
               }
               imgs[index].style.top = -(top-h*index)+'px'
           }catch(e){
               console.log(e)
           }

        }
    }
    top=null;
    index=null;
    imgs=null;
    h=null;
}
