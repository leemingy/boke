const base = 'http://leeminy.cn:3333'

const banerurl = base+"/showBanner";
const bgurl =  base+'/findBg';
const bannerDes = base+'/findBannerDes';
const codeUrl = base+'/findPageShare?page=0&num=7';
const codepage = base+'/findPageShare'
const codeDes = base+'/findShareDes';

const cxUrl = base+'/findPageCode?page=0&num=7'
const cxPage = base+'/findPageCode'
const cxDes = base+'/findCodeDes'
const cxall = base+'/findCode'

const lifeUrl = base+'/findPageLife?page=0&num=20'

const lifeUrli = base+'/findPageLife?page=0&num=7'
const lifePage = base+'/findPageLife'
const lifeDes = base+'/findLifeDes'
const gjUrl = base+"/findPageGj?page=0&num=7"
const gjPage= base+"/findPageGj"
const gjDes = base+"/findGjDes"
const allUrl = base+'/findPageArticle?page=0&num=7';
const alldes = base+"/findArticleDes"

const pageType = base+'/findPageTArticle'

const uinfoUrl = base+'/findUser';
const trendUrl = './mock/trends.json';
const linkUrl = base+'/findLink';
const varUrl = base+'/findType';
const hotUrl  = './mock/hot.json';
const rsUrl = './mock/rs.json';
const morebc = './mock/more/morebc.json';
const moregj = './mock/more/moregj.json';
const morelife = './mock/more/morelife.json';
const morers = './mock/more/morers.json';
const moretd = './mock/more/moretd.json';
const desUrl = './mock/des.json';
export {base,lifeUrli,alldes,cxall,gjPage,codepage,lifePage,cxPage,gjDes,lifeDes,gjUrl,pageType,bannerDes,banerurl,bgurl,codeUrl,codeDes,cxDes,cxUrl,uinfoUrl,trendUrl,allUrl,linkUrl,varUrl,hotUrl,rsUrl,lifeUrl,morebc,moregj,morelife,morers,moretd,desUrl}
