/* PIXI.JS circles */
    /* Author: Saifudeen */
    /* Pen: nisaifudeen*/
    $(document).ready(function () {
     // Global variables
      let type = "WebGL",
        Application = PIXI.Application,
        Graphics = PIXI.Graphics,
        Container = PIXI.Container,
        winWidth = window.innerWidth,
        winHeight = window.innerHeight,
        /* ********************************************** */  
        count = 400, //Circle's count
        /* ********************************************** */
        winValue,
        toTop = false,
        toBottom = false,
        toTopLeft = false,
        toTopRight = false,
        toBottomLeft = false,
        toBottomRight = false;

      const my = window.innerHeight / 2,
        mw = window.innerWidth / 2;


      //basic pixi support
      if (!PIXI.utils.isWebGLSupported()) {
        type = "canvas";
      }

      //Base PIXI app
      let app = new Application(winWidth, winHeight, {
          view: document.getElementById('my-canvas'),
          transparent: true,
          antialias: true
        }),

        //Container
        container = new Container(),
        circle;
      app.stage.addChild(container);
      container.interactive = true;

      for (i = 0; i <= count; i++) {
        rad = Math.floor(Math.random() * 60); //Radius
        xVal = Math.floor(Math.random() * winWidth); //Left value
        yVal = Math.floor(Math.random() * winHeight); //Top value
        //generateBubble(Radius, left value, top value)
        generateBubble(rad, xVal, yVal);
      }

      function generateBubble(rad, xVal, yVal) {
        //Bubbles
        circle = new Graphics();
        circle.lineStyle(1, 0xe5bf17);
        circle.beginFill(0x000000, 0);
        circle.drawCircle(xVal, yVal, rad);
        circle.interactive = true;
        circle.endFill();
        container.addChild(circle);
      }

      //Bubble float
      function bubbleFloat() {
        for (j = 0; j <= count; j++) {
          //Added TweenMax animation
          TweenMax.to(circle.parent.children[j], 60, {
            pixi: {
              x: "+=" + (Math.random() * 300),
              y: "+=" + (Math.random() * 200)
            },
            repeat: -1,
            yoyo: true
          });

        }
      }

      bubbleFloat();
     
      container.mousemove = function (v) {
        circle.parent.children.forEach(function (e) {
          e.mouseover = function (currentCircle) {
            let clientXInside = currentCircle.data.originalEvent.clientX,
              clientYInside = currentCircle.data.originalEvent.clientY;
            
            if (clientXInside < mw) {
              if (clientYInside < my) {
                //Left top area
                // bubbleMove(element, time, x value, y value, x+=, y-=); for tweenmax
                bubbleMove(currentCircle.target, 0.9, 200, 100, 1, 1);
              } else {
                //Left bottom area
                // bubbleMove(element, time, x value, y value, x+=, y-=); for tweenmax
                bubbleMove(currentCircle.target, 0.9, 200, 100, 1, -1)
              }
            } else {
              if (clientYInside < my) {
                //Right top area
                // bubbleMove(element, time, x value, y value, x+=, y-=); for tweenmax
                bubbleMove(currentCircle.target, 0.9, 200, 100, -1, 1)
              } else {
                //Right bottom area
                bubbleMove(currentCircle.target, 0.9, 200, 100, -1, -1)
              }

            }

          }
        })
      }

      function bubbleMove(elem, t, xV, yV, posX, posY) {
        elem.tint = 0x00ff00;
        TweenMax.to(elem, t, {
          pixi: {
            x: "+=" + (Math.random() * (xV * posX)),
            y: "+=" + (Math.random() * (yV * posY))
          }
        });

        setTimeout(function () {
          elem.tint = 0xe5bf17;

        }, 1000)
      }

      document.ondblclick = function () {
        circle.parent.children.forEach(function (e) {
          TweenMax.to(e, 0.7, {
            pixi: {
              x: (Math.random() * 100),
              y: (Math.random() * 100)
            }
          });
        });
      }
    })